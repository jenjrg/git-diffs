import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withNavigation, actions as navigationActions } from '@db-mobile-banking/react-native-navigation';
import { View, StyleSheet, Image, Dimensions } from 'react-native';
import { Theme, themes, Text, Button, colors, TouchableOpacity, Spinner } from '@db-mobile-banking/react-native-user-interface';
import * as otpActions from 'ebanking-area-otp/actions';
import * as actionCreators from 'ebanking-area-dot/actions';
import * as otpUtils from 'ebanking-area-otp/utils';
import * as selectors from '../selectors';
import PropTypes from 'prop-types';
import OtpSwitcher from './OtpSwitcher';

const windowHeight = Dimensions.get('window').height;

export const danskeNarrowSemiBold = 'danskenarrow_semibold';
const nemIdLogo = require('../../../assets/nemIDLogo.png');
const iconPhone = require('../../../assets/iconPhone.png');

class NemIdAppPure extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    cancelNemId: PropTypes.func.isRequired,
    getPossesionChallenge: PropTypes.func.isRequired,
    isLoading: PropTypes.bool,
    theme: PropTypes.object.isRequired
  };

  render() {
    const backgroundColorStyle = {
      backgroundColor: this.props.theme.backgroundColor
    };
    return (
      <View style={styles.mainContainer}>
        <View style={[backgroundColorStyle, styles.floatingContainer]}>
          <View style={styles.logoContainer}>
            <Text style={styles.headerTitle} textId='areas.otp.nemIdScreenTitle' />
            <View style={styles.headerLogo}>
              <Image style={styles.logo} source={nemIdLogo} />
            </View>
            <View style={[styles.container, styles.phoneIconWrapper]}>
              <Image style={styles.phoneIcon} source={iconPhone} />
              <Text style={styles.phoneText} textId='areas.eSigning.eSignatureApp' />
            </View>
            <View style={styles.textInfo}>
              <Text style={styles.text} textId='areas.eSigning.eSignatureText' />
            </View>
            <View style={styles.buttonContainer}>
              <Button
                style={styles.sendButton}
                testID={'areas.eSigning.eSignatureSend'}
                onActivated={this.props.getPossesionChallenge}
              >
                <Text style={styles.buttonText} textId='areas.otp.nemIdSend' />
              </Button>
            </View>
            <OtpSwitcher />
            <View>
              <TouchableOpacity
                testID='areas.otp.codeCard.close'
                onPress={this.props.cancelNemId}
              >
                <Text
                  textId='areas.otp.codeCard.close'
                  style={[styles.textLink, styles.closeLink]}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      {this.props.isLoading ? (
        <View style={styles.spinnerContainer}>
          <Spinner />
        </View>
      ) : null}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoading: selectors.getIsNemIdLoading(state)
  };
}

const actionsToProps = {
  ...actionCreators,
  ...otpActions,
  ...navigationActions,
  ...otpUtils
};

const NemIdApp = connect(
  mapStateToProps,
  actionsToProps
)(Theme(themes.lightGray)(withNavigation(NemIdAppPure)));

export default NemIdApp;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  floatingContainer: {
    height: windowHeight <= 550 ? 550 : 610
  },
  spinnerContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.8)'
  },
  headerTitle: {
    height: 18,
    fontSize: 16,
    lineHeight: 18.0,
    color: colors.primary.blue[0]
  },
  headerLogo: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 20
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 30,
    marginBottom: windowHeight <= 550 ? 0 : 60
  },
  logo: {
    marginBottom: 10
  },
  text: {
    textAlign: 'center',
    marginBottom: 10
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 26
  },
  phoneIconWrapper: {
    height: 50
  },
  phoneIcon: {
    resizeMode: 'contain'
  },
  phoneText: {
    paddingLeft: 20,
    fontSize: 14,
    color: colors.secondary.blue[3],
    textAlign: 'center'
  },
  sendButton: {
    marginBottom: 20,
    backgroundColor: colors.primary.action[0]
  },
  textInfo: {
    paddingHorizontal: 50,
    minHeight: 93
  },
  textLink: {
    color: colors.primary.action[0],
    width: '100%',
    textAlign: 'center',
    paddingHorizontal: 10
  },
  buttonText: {
    color: colors.primary.white[0]
  },
  closeLink: {
    marginTop: 40
  }
});
