export const SET_CODE_CARD_PIN_CODE = 'otp/SET_CODE_CARD_PIN_CODE';
export const RESET_CODE_CARD_PIN_CODE = 'otp/RESET_CODE_CARD_PIN_CODE';
export const SET_CALLBACK = 'otp/SET_CALLBACK';
export const SET_REQUEST_CONFIG = 'otp/SET_REQUEST_CONFIG';
export const SET_REQUEST_URL = 'otp/SET_REQUEST_URL';
export const ENTER_CODE_CARD_PIN_CODE = 'otp/enter_code_card_pin_code';
export const ENTER_CODE_CARD_PIN_CODE_DONE = 'otp/enter_code_card_pin_code_done';
export const CANCEL = 'otp/cancel';
export const DO_STEP_UP = 'otp/do_step_up';
export const DO_STEP_UP_DONE = 'otp/do_step_up_done';
export const CODE_CARD_CLOSED = 'otp/code_card_closed';
export const SET_STATUS = 'otp/set_status';
export const CLEAR_STATE = 'otp/clear_state';
export const GET_OTP_DEVICE_LIST = 'otp/get_otp_device_list';
export const SET_DEVICE_LIST = 'otp/set_device_list';
export const SET_SELECTED_DEVICE_INDEX = 'otp/set_selected_device_index';
export const SWITCH_SELECTED_DEVICE = 'otp/switch_selected_device';
export const SWITCH_TO_NEMID = 'otp/switch_to_nemid';
export const SWITCH_TO_DANSKEID = 'otp/switch_to_danskeid';
export const VALIDATE_OTP_ATTEMPT_COUNT = 'otp/validate_otp_attempt_count';
export const INCREASE_OTP_ATTEMPT_COUNT = 'otp/increase_otp_attempt_count';
export const CONTINUE_OTP = 'otp/continue_otp';
export const SET_DEVICE_DATA = 'otp/set_device_data';
export const SET_SECURITY_LEVEL = 'otp/set_security_level';
export const NAVIGATE_TO_NEM_ID_SCREEN = 'otp/navigate_to_nem_id_screen';
export const NAVIGATE_TO_CODE_CARD_SCREEN = 'otp/navigate_to_code_card_screen';
export const NAVIGATE_BACK = 'otp/navigate_back';
export const POSSESION_CHALLENGE_REQUEST = 'otp/get_possesion_challenge';
export const SET_IS_NEMID_LOADING = 'otp/set_is_nemid_loading';
export const CANCEL_NEMID = 'otp/cancel_nemid';
export const DANSKEID_CHALLENGE_REQUEST = 'otp/get_danskeid_challenge';
export const CANCEL_DANSKEID = 'otp/cancel_danskeid';
export const SET_POLL_URL = 'otp/set_poll_url';
export const SET_TOKEN = 'otp/set_token';
export const SET_ERROR_COUNT = 'otp/set_error_count';
export const CANCEL_AND_LOGOFF = 'otp/cancel_and_logoff';
export const OTP_DATA_VALIDATION_DONE = 'otp/otp_data_validation_done';
export const DANSKEID_INIT_OTP = 'otp/otp_init_danskeid';
export const SET_OTP_REQUEST_ID = 'otp/otp_set_request_id';
export const IS_DANSKE_ID_ENROLLED = 'otp/is_danske_id_enrolled';
export const HANDLE_SECOND_FACTOR_INPUT_FOR_TRANSFER = 'otp/handle_second_factor_input_for_transfer';
export const PROCESS_SECOND_FACTOR_FOR_TRANSFER = 'otp/process_second_factor_for_transfer';

export function otpDataValidationSuccess(otpData) {
  return {
    type: OTP_DATA_VALIDATION_DONE,
    otpData
  };
}

export function otpDataValidationFailed(error) {
  return {
    type: OTP_DATA_VALIDATION_DONE,
    error
  };
}

export function cancelAndLogoff() {
  return {
    type: CANCEL_AND_LOGOFF
  };
}

export function setIsNemIdLoading(isLoading) {
  return {
    type: SET_IS_NEMID_LOADING,
    isLoading
  };
}

export function cancelNemId() {
  return {
    type: CANCEL_NEMID
  };
}

export function cancelDanskeId() {
  return {
    type: CANCEL_DANSKEID
  };
}

export function setDeviceData(deviceNumber, codeNumber, deviceType) {
  return {
    type: SET_DEVICE_DATA,
    deviceNumber,
    codeNumber,
    deviceType
  };
}

export function setCodeCardPinCode(code) {
  return {
    type: SET_CODE_CARD_PIN_CODE,
    code: code
  };
}

export function resetCodeCardPinCode() {
  return {
    type: RESET_CODE_CARD_PIN_CODE
  };
}

export function setCallback(successCallback, failureCallback) {
  return {
    type: SET_CALLBACK,
    successCallback,
    failureCallback
  };
}

export function setRequestConfig(requestConfig) {
  return {
    type: SET_REQUEST_CONFIG,
    requestConfig
  };
}

export function setRequestUrl(requestUrl) {
  return {
    type: SET_REQUEST_URL,
    requestUrl
  };
}

export function enterCodeCardPinCode(code) {
  return {
    type: ENTER_CODE_CARD_PIN_CODE,
    code: code
  };
}

export function cancel() {
  return {
    type: CANCEL
  };
}

export function enterCodeCardPinCodeDone() {
  return {
    type: ENTER_CODE_CARD_PIN_CODE_DONE
  };
}

export function getOtpDeviceList() {
  return {
    type: GET_OTP_DEVICE_LIST
  };
}

export function setDeviceList(value) {
  return {
    type: SET_DEVICE_LIST,
    value: value
  };
}

export function switchSelectedDevice(deviceType) {
  return {
    type: SWITCH_SELECTED_DEVICE,
    deviceType
  };
}

export function switchToNemId() {
  return {
    type: SWITCH_TO_NEMID
  };
}

export function switchToDanskeId() {
  return {
    type: SWITCH_TO_DANSKEID
  };
}

export function setSelectedDeviceIndex(value) {
  return {
    type: SET_SELECTED_DEVICE_INDEX,
    value: value
  };
}

export function setStatus(value) {
  return {
    type: SET_STATUS,
    value: value
  };
}

export function codeCardClosed() {
  return {
    type: CODE_CARD_CLOSED
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  };
}

export function validateOtpAttemptCount() {
  return {
    type: VALIDATE_OTP_ATTEMPT_COUNT
  };
}

export function increaseOtpAttemptCount() {
  return {
    type: INCREASE_OTP_ATTEMPT_COUNT
  };
}

export function doStepUp(response, url, config) {
  return {
    type: DO_STEP_UP,
    response,
    url,
    config
  };
}

export function doStepUpSuccess(result) {
  return {
    type: DO_STEP_UP_DONE,
    result,
    error: null
  };
}

export function doStepUpError(error) {
  return {
    type: DO_STEP_UP_DONE,
    error
  };
}

export function continueOtp() {
  return {
    type: CONTINUE_OTP
  };
}

export function setSecurityLevel(value) {
  return {
    type: SET_SECURITY_LEVEL,
    value
  };
}

// TODO: remove in refactoring
export function navigateToNemIdScreen() {
  return {
    type: NAVIGATE_TO_NEM_ID_SCREEN
  };
}

// TODO: remove in refactoring
export function navigateToCodeCardScreen() {
  return {
    type: NAVIGATE_TO_CODE_CARD_SCREEN
  };
}

export function getPossesionChallenge() {
  return {
    type: POSSESION_CHALLENGE_REQUEST
  };
}

export function getDanskeIdChallenge() {
  return {
    type: DANSKEID_CHALLENGE_REQUEST
  };
}

// TODO: remove in refactoring
export function navigateBack() {
  return {
    type: NAVIGATE_BACK
  };
}

export function setPollUrl(value) {
  return {
    type: SET_POLL_URL,
    value
  };
}

export function setToken(value) {
  return {
    type: SET_TOKEN,
    value
  };
}

export function setErrorCount(value) {
  return {
    type: SET_ERROR_COUNT,
    value
  };
}

export function danskeIdInitOTP() {
  return {
    type: DANSKEID_INIT_OTP
  };
}

export function setOtpRequestId(value) {
  return {
    type: SET_OTP_REQUEST_ID,
    value
  };
}

export function isDanskeIdEnrolled(isEnrolled) {
  return {
    type: IS_DANSKE_ID_ENROLLED,
    isEnrolled
  };
}

export function handleSecondFactorInputForTransfer(value) {
  return {
    type: HANDLE_SECOND_FACTOR_INPUT_FOR_TRANSFER,
    value
  };
}

export function processSecondFactorForTransfer() {
  return {
    type: PROCESS_SECOND_FACTOR_FOR_TRANSFER
  };
}
